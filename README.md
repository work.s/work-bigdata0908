# work-bigdata0908

问题1：解决小问题

- 解决办法1：减少spark.sql.shuffle.partitions配置的值。优点：这种改法简单粗暴；缺点：这种改法会作用于每次map转reduce的shuffle操作。但是时间过程中spark任务会拥有多次洗牌操作，在最后一次之前的shuffle操作也会由于分区的值减少了导致任务并行度降低，会导致sql执行更久；
- 解决办法2：通过调用coalesce(num)函数重新进行分区。优点：可以根据需要对数据进行分区，灵活性比第一种更好；缺点：分区过程中避免不了数据倾斜，无法做到根据实时数据量动态调整分区的数量。
- 解决办法3：调查中

问题2：实现Compact table command